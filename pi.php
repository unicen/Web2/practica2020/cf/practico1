<html>
<p>π (pi) es la relación entre la longitud de una circunferencia y su diámetro en geometría euclidiana. Es un número irracional2​ y una de las constantes matemáticas más importantes. Se emplea frecuentemente en matemáticas, física e ingeniería. El valor numérico de π, truncado a sus primeras cifras, es el siguiente.<p>

<p>π = 3 , 141 592 653 589 793 238 462 … </p>
<p>pi() = <?=pi()?></p>
<p>M_PI = <?=M_PI?></p>

<p>El valor de π se ha obtenido con diversas aproximaciones a lo largo de la historia, siendo una de las constantes matemáticas que más aparece en las ecuaciones de la física, junto con el número e. Cabe destacar que el cociente entre la longitud de cualquier circunferencia y la de su diámetro no es constante en geometrías no euclidianas.</p>
</html>