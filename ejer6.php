<!--
    TODO:
    Imprimir la tabla de multiplicar generada automáticamente en PHP. 
     * Funcion que multiplique fila por columna
     * recorrer filas
      * recorrer columnas
    Modifique el ejercicio para que el límite de la tabla sea ingresado por un usuario.
     * Formulario para que el usuario ingrese un valor
     * Leer el valor en php
     * Modificar la funcion anterior para el limite sea dianamico
     * Unir valor leído con función
-->
<?php
    function producto($fila, $columna){
        return $fila * $columna;
    }

    function mostrarTabla($limite){
        $tabla = "<table>";
        for($fila = 1;$fila<$limite;$fila++){
            $tabla.="<tr>";
            for($columna=1;$columna<$limite;$columna++){
                $producto = producto($fila,$columna);
                $tabla.= "<td>" . $producto . "</td>";
            }
            $tabla.="</tr>";
        }
        $tabla.="</table>";
        return $tabla;
    }

    function leerLimite(){
        $limite = 10;
        // Vemos la variable $_GET
        return $limite;
    }

    $limite = leerLimite();
    $tabla = mostrarTabla($limite);
?>
<html>
    <body>
        <div style="border: 1px solid">
            <?=$tabla?>
        </div>
    </body>
</html>