<!--
    Modifique el ejercicio 2 para que el usuario mediante links html pueda ir modificando el tamaño de la lista generada. 

    TODO:
    * Capturar la cantidad desde GET.
    * Adaptar la funcion listaHTML para que muestre un limite de elementos.

-->
<!--
    Escribir un programa que muestre una lista html generada desde el servidor a través de un arreglo. 
-->
<?php
    function elementoListaHTML($elemento){
        return "<li>" . $elemento . "</li>";
    }

    function listaHTML($arreglo, $limite){
        $elementos_html = "";
        
        $cant_elementos = count($arreglo);

        for($i = 0; $i < $cant_elementos && $i < $limite; $i++){
            $elemento = $arreglo[$i];
            $elementos_html = $elementos_html . elementoListaHTML($elemento);
        }
        
        return "<ol>" . $elementos_html . "</ol>";
    }


    //$arreglo = array();
    $arreglo = [1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0];
    
    $limite = count($arreglo);

    if(isset($_GET) && count($_GET)>0){
        if(isset($_GET["cantidad"]) && $_GET["cantidad"] != ""){
            $cantidad = $_GET["cantidad"];
            echo "<p>Cantidad a mostrar: " . $cantidad . "<br>";
            $limite = $cantidad;
        }
    }
    echo listaHTML($arreglo, $limite);
?>
<a href="ejer4.php?cantidad=5">Mostrar  5 elementos</a><br> 
<a href="ejer4.php?cantidad=10">Mostrar 10 elementos</a><br> 
<a href="ejer4.php?cantidad=15">Mostrar 15 elementos</a><br>
<a href="ejer4.php">Mostrar todos</a><br> 