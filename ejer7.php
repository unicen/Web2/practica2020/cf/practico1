<!--
    Una persona desea invertir dinero en un banco, el cual le otorga un % de interés mensual. 
    Escribir un programa para simular la inversión que imprima cuál será la cantidad de dinero que esta persona tendrá mes a mes durante un año. 
    Genere una tabla HTML para mostrar el resultado.

    TODO:
    -1. formulario <- capital, iterés, tiempo
    -2. function gananciaMensual(capital, interes) -> ganacia
    -3. function interesCompuesto(capital, interes, meses) -> array() mes => monto
    4. leer el form y llamar a las funciones

-->
<?php
    function gananciaMensual($capital, $interes){
        return $capital * $interes / 100;
    }

    function interesCompuesto($capital, $interes, $meses){
        $monto = array();
        $monto[0] = $capital;

        for($mes = 1; $mes <= $meses; $mes++){
            $gananciaMes = gananciaMensual($monto[$mes-1], $interes);
            $monto[$mes] = $monto[$mes-1] + $gananciaMes;
        }

        return $monto;
    }

    if(isset($_GET) && count($_GET)>0){

        if(isset($_GET["capital"]) && is_numeric($_GET["capital"])){
            $capital = $_GET["capital"];
        }else{
            echo "<p>No ingresó capital</p>";
        }

        if(isset($_GET["interes"]) && is_numeric($_GET["interes"])){
            $interes = $_GET["interes"];
        }else{
            echo "<p>No ingresó interes</p>";
        }

        if(isset($_GET["tiempo"]) && is_numeric($_GET["tiempo"])){
            $tiempo = $_GET["tiempo"];
        }else{
            $tiempo = 12;
        }

       
        if(isset($capital) && isset($interes) && isset($tiempo)){
            $dineroMesAMes = interesCompuesto($capital, $interes, $tiempo);
            //print_r($dineroMesAMes);
        }
    }
?>
<html>
    <body>
        <form>
            <p>Capital: <input type="number" name="capital" /></p>
            <p>Interés: <input type="number" name="interes" /></p>
            <p>Tiempo: <input type="number" name="tiempo" /></p>
            <p><input type="submit" /></p>
        </form>
        <?php if(isset($dineroMesAMes)): ?>
            <table style="border: 1px solid grey;">
                <tr>
                    <!-- MES -->
                    <?php for($mes = 0; $mes < count($dineroMesAMes); $mes++): ?>
                        <td  style="border: 1px solid grey;"><?=$mes?></td>
                    <?php endfor; ?>
                </tr>
                <tr>
                    <!-- Monto total -->
                    <?php for($mes = 0; $mes < count($dineroMesAMes); $mes++): ?>
                        <td  style="border: 1px solid grey;"><?=$dineroMesAMes[$mes]?></td>
                    <?php endfor; ?>
                </tr>
            </table>
        <?php endif; ?>
    </body>
<html>

