<!--
    Crear una calculadora básica server side. 
    
    Esta calculadora debe permitir generar operaciones básicas dado dos números leídos desde un formulario.
    Ademas, se deberá incluir una barra de navegación para 
        :Acceder a una sección número pi: esta sección debe mostrar mostrar una descripción de lo que representa este número y su valor. 
            Investigue diferentes formas de obtener este valor en PHP.
    Acceder a una sección about que indique los creadores de la calculadora. 
    Si le indicamos con un parametro un desarrollador en particular debe mostrar solo eso: 
    ejemplo: 
            <?php if(isset($developer) && $developer != "fecorvalan"): ?>

    Paginas:   
    * form calculadora calculadora.php
    TODO:
     - Formulario
     - Leer $_GET/$_POST
     - Calcular y mostrar resultados.
    * pagina sobre PI pi.php
    * about.php


-->
<?php
    if(isset($_GET) && count($_GET)>0){

        if(isset($_GET["n"]) && is_numeric($_GET["n"])){
            $n = $_GET["n"];
        }else{
            echo "<p>No ingresó el primer número</p>";
        }

        if(isset($_GET["m"]) && is_numeric($_GET["m"])){
            $m = $_GET["m"];
        }else{
            echo "<p>No ingresó el segundo número</p>";
        }

        if(isset($_GET["operacion"])){
            $operacion = $_GET["operacion"];
        }else{
        }
       
        if(isset($m) && isset($n) && isset($operacion)){
            switch ($operacion) {
                case '+':
                    $result = $m + $n;
                    break;
                case '-':
                    $result = $m - $n;
                    break;
                case 'x':
                    $result = $m * $n;
                    break;
                case '/':
                    $result = $m / $n;
                    break;
                default:
                    # code...
                    break;
            }
        }
    }
?>
<html>
    <body>
        <a href="pi.php">PI</a> <a href="about.php">Creadores</a>
        <form>
            <p><input type="number" name="n" /> <input type="number" name="m" /></p> 
            <p><input type="submit" name="operacion" value="+" /> <input type="submit" name="operacion" value="-" /></p>
            <p><input type="submit" name="operacion" value="x" /> <input type="submit" name="operacion" value="/" /></p>
        </form>
        <?php if(isset($result)): ?>
            <p>El resultado es <?=$result?></p>
        <?php endif; ?>
    </body>
<html>