<?php 

    function mostrarDesarrollador($id, $desarrollador){
        $nombre =  $desarrollador["nombre"];
        $descripcion = $desarrollador["desc"];
        $html = "<div>
                    <a href=\"about.php?developer=$id\"><h2>$nombre</h2></a>
                    <p>$descripcion</p>
                </div>";
        return $html;
    }

    function mostrarDesarrolladores($desarolladores){
        $html = "";
        foreach ($desarolladores as $id => $desarrollador) {
            $html .= mostrarDesarrollador($id, $desarrollador);
        }
        return $html;
    }

    $desarolladores = [
        "ndazeo" => [
            "nombre" => "Nicolás Dazeo",
            "desc" => "Escribió esta pagina."
        ],
        "fecorvalan" => [
            "nombre" => "Facundo Emanuel Corvalán",
            "desc" => "Propuso paginas separadas."
        ],
        "aabella" => [
            "nombre" => "Aaron Abella",
            "desc" => "Propuso paginas separadas."
        ],
        "mmenchon" => [
            "nombre" => "Magalí Menchón",
            "desc" => "Encontró un error. Propuso la función pi()."
        ],
    ];

    if(isset($_GET) && count($_GET)>0 && isset($_GET["developer"]) && $_GET["developer"] != ""){
        $id = $_GET["developer"];
        $desarrollador = $desarolladores[$id];
        
        $resultado = mostrarDesarrollador($id, $desarrollador);
    }else{
        $resultado = mostrarDesarrolladores($desarolladores);
    }

?>
<html>
    <body>
        <?=$resultado?>
    </body>
</html>


