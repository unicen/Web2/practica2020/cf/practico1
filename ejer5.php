<!--
    Construir un programa que calcule el índice de masa corporal de una persona (IMC = peso [kg] / altura [m]2) 
        e informe el estado en el que se encuentra esa persona en función del valor de IMC.
    TODO:
     * Una funcion que calcule el IMC.
     * Dado el IMC indique el estado.
     * Formulario que pida peso y altura.
-->
<?php 
    function IMC($peso, $altura){
        $alturaM = ($altura/100);
        return $peso / ($alturaM * $alturaM);
    }

    function estado($IMC){
        if($IMC<18.5){
            return "Bajo peso";
        }else if($IMC<25){
            return "Normal";
        }else if($IMC<30){
            return "Sobrepeso";
        }else{
            return "Obesidad";
        }
    }

    if(isset($_GET) && count($_GET)>0){

        if(isset($_GET["peso"]) && $_GET["peso"] != ""){
            $peso = $_GET["peso"];
        }else{
            echo "<p>No ingresó peso</p>";
        }

        if(isset($_GET["altura"]) && $_GET["altura"] != ""){
            $altura = $_GET["altura"];
        }else{
            echo "<p>No ingresó altura</p>";
        }

        if(isset($peso) && isset($altura)){
            $IMC = IMC($peso, $altura);
            $estado = estado($IMC);
            echo "<p>Su estado es " . $estado . " (IMC= $IMC)</p>";
        }
    }
?>
<html>
<body>
<form>
    <p>Peso: <input type="number" name="peso" />Kg</p>
    <p>Altura: <input type="number" name="altura" /> cm</p>
    <p><input type="submit" /></p>
</form>
</body>
<html>

